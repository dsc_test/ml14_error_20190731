include default.mk

default:  ML14_ERROR.exe 
	@echo     ML14_ERROR

ML14_ERROR.inf:  ML14_ERROR.obj ddmodel14.obj

clean:
	if exist *.obj rm -f *.obj
	if exist *.???~ rm -f *.???~
	if exist *.lib rm -f *.lib
	if exist *.mod rm -f *.mod
	if exist *.exe rm -f *.exe
	rm -f compall.txt