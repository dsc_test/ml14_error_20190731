      SUBROUTINE ddmodel14(av)  
        
        ! Vars in
        INTEGER, INTENT(IN) :: av

        ! Vars here (usually from various include files)
        INTEGER origzn, destzn, JZUSE, iin1, rdunit, stat
        INTEGER,PARAMETER ::                             
     $              MXZMA   = 1713,
     $              LXTZN   = 1714,
     $              JZTF    = -327,
     $              MXAV    =  191        
        
        ! INCLUDE here as above from other inc files
        INCLUDE 'relocmon.inc'

        LOGICAL(KIND=1), DIMENSION(0:lxtzn,JZTF:JZTF) :: zonedef
        REAL,DIMENSION(lxtzn,lxtzn,mxav) :: ddfuncval
        CHARACTER(LEN=1) :: Tin
        CHARACTER(LEN=1), DIMENSION(72) :: carr
        CHARACTER(LEN=8) :: FN
        REAL(KIND=2) :: RANDOM@

        ! Need fully-modelled zones only
        JZUSE = JZTF

        ! Initialise arrays
        WRITE(6,*)' Initialising arrays.'
        DO destzn=1,lxtzn
          zonedef(destzn,JZTF) = .FALSE.
          znda(destzn,JTMPREL) = RANDOM@()
          zndaav(av,destzn,JTMPREL) = RANDOM@()
          DO origzn=1,lxtzn
            znznda(origzn,destzn,JTMPREL) = RANDOM@()
            znzndaav(av,origzn,destzn,JTMPREL) = RANDOM@()
            ddfuncval(origzn,destzn,av) = RANDOM@()
          END DO
        END DO
        WRITE(6,*)'Arrays initialised with pseudo-random reals'

        ! Read in zone definitions (assumes zone.def in cwd)
        FN = 'zone.def'
        
8001    FORMAT(A1,I4,A1,A1)

        rdunit = 7
        OPEN(UNIT=rdunit,FILE=FN)
        
        DO
          READ(UNIT=rdunit,FMT=8001,IOSTAT=stat)
     $    	carr(1),iin1,carr(2),Tin
          
          ! Exit loop at last line
          IF (carr(1) == '0') EXIT
          
          IF (Tin=='F') THEN
            zonedef(iin1,JZTF) = .TRUE.
          ENDIF
        END DO

        CLOSE(UNIT=rdunit,IOSTAT=stat)

        ! Begin error loop
        WRITE(6,*)'Beginning error loop'
        DO origzn=1,mxzma
          IF( zonedef(origzn,JZUSE) )THEN
            DO destzn=1,mxzma
              IF (zonedef(destzn,JZUSE)) THEN

                znznda(origzn,destzn,JTMPREL)=
     $            znda(destzn,JTMPREL)*
     $            ddfuncval(origzn,destzn,av)

                znznda(origzn,lxtzn,JTMPREL)=
     $            znznda(origzn,lxtzn,JTMPREL)+
     $            znznda(origzn,destzn,JTMPREL)

                znzndaav(av,origzn,destzn,JTMPREL)=
     $            zndaav(av,destzn,JTMPREL)*
     $            ddfuncval(origzn,destzn,av)           

                ! This is where the error occurs
                znzndaav(av,origzn,lxtzn,JTMPREL)=
     $            znzndaav(av,origzn,lxtzn,JTMPREL)+
     $            znzndaav(av,origzn,destzn,JTMPREL)

              ENDIF
            ENDDO
          ENDIF
        ENDDO
      END
