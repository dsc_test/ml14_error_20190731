      INTEGER JTMPREL
      PARAMETER(JTMPREL=1003)
      REAL znda(lxtzn,JTMPREL:JTMPREL)
      REAL zndaav(1:mxav,lxtzn,JTMPREL:JTMPREL)      
      REAL,dimension (lxtzn,lxtzn,JTMPREL:JTMPREL):: znznda
      REAL mlrlsum(lxtzn,lxtzn)

      REAL,dimension (1:mxav,lxtzn,lxtzn,JTMPREL:JTMPREL):: znzndaav

      COMMON /DDMON/ znda, znznda, mlrlsum,znzndaav, zndaav
