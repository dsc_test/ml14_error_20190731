.SUFFIXES: .obj .for .exe .f95 .dll .inf .mod64

.for.obj:
	ftn95 $< /64 /defint_kind 3 /deflog_kind 1 /no_com /fu /im /mod_path \source\mods /cfpp /define DEBUG 1 $(ARGS) >>compall.txt
	
.f95.obj:
	ftn95 $< /64 /defint_kind 3 /deflog_kind 1 /no_com /fu /im /mod_path \source\mods /cfpp /define DEBUG 1 $(ARGS) >>compall.txt

.f95.mod64: 
	ftn95 $< /64 /defint_kind 3 /deflog_kind 1 /no_com /fu /im /mod_path \source\mods /cfpp /define DEBUG 1 $(ARGS) >>compall.txt

.for.mod64:
	ftn95 $< /64 /defint_kind 3 /deflog_kind 1 /no_com /fu /im /mod_path \source\mods /cfpp /define DEBUG 1 $(ARGS) >>compall.txt

.inf.exe:
	slink64 $< >>\slink64.txt

default:

.PHONY: clean

clean:
	if exist *.obj rm -f *.obj
	if exist *~ rm -f *~
	if exist *.lib rm -f *.lib
	if exist *.mod64 rm -f *.mod64
	if exist *.exe rm -f *.exe
